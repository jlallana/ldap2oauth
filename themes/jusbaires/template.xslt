<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:template match="/">
        <html>
            <head>
                <meta charset="utf-8" />
                <link rel="stylesheet" type="text/css" href="themes/jusbaires/styles/default.css" />
            </head>
            <body>
                <img src="themes/jusbaires/images/logo.png"></img>
                <form method="POST">
                    <input type="text" placeholder="usuario" name="username" />
                    <input type="password" placeholder="contraseña" name="password" />
                    <input type="submit" value="login" />
                </form>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>