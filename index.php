<?php

if($_SERVER["REQUEST_METHOD"] == "POST") {
    
    $username = $_POST["username"];
    $password = $_POST["password"];

    $ldap = ldap_connect('ldap.forumsys.com');

    ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
    
    $status = @ldap_bind($ldap, "uid=$username,dc=example,dc=com", "$password");
    
    
    if($status) {
    
        $result = ldap_read($ldap, "uid=$username,dc=example,dc=com", "(uid=$username)");
        
        $entries = ldap_get_entries($ldap, $result);
        
        echo $entries[0]["cn"][0];
    } else {
        $error = true;
    }
}

header("content-type: text/xml");

?>
<?xml version="1.0"?>
<?xml-stylesheet type="text/xsl" href="themes/jusbaires/template.xslt"?>
<login></login>